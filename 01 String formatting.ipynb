{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# String formatting\n",
    "\n",
    "Being able to print things to the screen is a fundamental part of interacting with a programming language. So far, we've kept it simple by passing the data we want to print directly to the `print` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "42\n"
     ]
    }
   ],
   "source": [
    "my_num = 42\n",
    "\n",
    "print(my_num)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We made things a bit nicer by using the fact that if you pass multiple arguments to `print` then it will print each of them, separated by spaces, allowing us to combine our data with a message:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "My num is 42\n"
     ]
    }
   ],
   "source": [
    "my_num = 42\n",
    "\n",
    "print(\"My num is\", my_num)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This works perfectly well but firstly you're using the implicit space that's added as part of your sentence and secondly you're passing in two separate pieces of information where they are logically one.\n",
    "\n",
    "What we can do instead is create a single string which contains the message we want to print and put special placeholders inside it where we want our data to appear. There's a few different ways to do this in Python (an older but still valid method you may see uses `%` and another uses a `format()` function) but for this course we will use the method called *f-strings* which was introduced in Python 3.6 (released December 2016).\n",
    "\n",
    "If you put a single `f` directly in front of the string that you are creating, it will enable a special string mode which lets you place `{}` inside the string with a variable name between the `{` and the `}`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "My num is 42\n"
     ]
    }
   ],
   "source": [
    "my_num = 42\n",
    "\n",
    "print(f\"My num is {my_num}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can have as many interpolations as you like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The answer is 42 and pi is 3.14159\n"
     ]
    }
   ],
   "source": [
    "answer = 42\n",
    "pi = 3.14159\n",
    "\n",
    "print(f\"The answer is {answer} and pi is {pi}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can even put more complex things in the `{}`, such as dictionary key access:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The answer is 42 and pi is 3.14159\n"
     ]
    }
   ],
   "source": [
    "my_dict = {\"answer\": 42, \"pi\": 3.14159}\n",
    "\n",
    "print(f\"The answer is {my_dict['answer']} and pi is {my_dict['pi']}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we had to use single quotes (`'`) when writing the dictionary key name (i.e. `'answer'`) as using double quotes would accidentally close the outer string."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "For all the exercises in this course, we are going to be working on some Python code which converts to and from [Morse Code](https://simple.wikipedia.org/wiki/Morse_code). In each section we will add or change the code so make sure that you don't skip any exercises.\n",
    "\n",
    "- Copy the following code in the text editor into a script called `encode.py` and run it in the terminal with `python encode.py` to check that it works.\n",
    "\n",
    "  ```python\n",
    "  letter_to_morse = {\n",
    "      'a':'.-', 'b':'-...', 'c':'-.-.', 'd':'-..', 'e':'.', 'f':'..-.', \n",
    "      'g':'--.', 'h':'....', 'i':'..', 'j':'.---', 'k':'-.-', 'l':'.-..', 'm':'--', \n",
    "      'n':'-.', 'o':'---', 'p':'.--.', 'q':'--.-', 'r':'.-.', 's':'...', 't':'-',\n",
    "      'u':'..-', 'v':'...-', 'w':'.--', 'x':'-..-', 'y':'-.--', 'z':'--..',\n",
    "      '0':'-----', '1':'.----', '2':'..---', '3':'...--', '4':'....-',\n",
    "      '5':'.....', '6':'-....', '7':'--...', '8':'---..', '9':'----.', ' ':'/'\n",
    "  }\n",
    "\n",
    "  message = \"SOS We have hit an iceberg and need help quickly\"\n",
    "\n",
    "  morse = []\n",
    "\n",
    "  for letter in message:\n",
    "      letter = letter.lower()\n",
    "      morse.append(letter_to_morse[letter])\n",
    "\n",
    "  # We need to join together Morse code letters with spaces\n",
    "  morse_message = \" \".join(morse)\n",
    "  \n",
    "  print(f\"Incoming message: {message}\")\n",
    "  print(f\"   Morse encoded: {morse_message}\")\n",
    "  ```\n",
    "- [<small>answer</small>](answer_morse_string_formatting.ipynb)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
