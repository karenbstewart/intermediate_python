{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Handling errors\n",
    "\n",
    "*Exceptions* are Python's error-handling system. You've probably come across exceptions as that's how Python lets you know when you've done something wrong! Until now, when one of those errors occured, Python would exit and the error message would be printed to the screen but this chapter is going to teach you how to harness these exceptions to make them work for you.\n",
    "\n",
    "Let's start with our `Dog` class again:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Dog:\n",
    "    def __init__(self, name, colour):\n",
    "        self.name = name\n",
    "        self.colour = colour\n",
    "        self.energy = 1\n",
    "    \n",
    "    def describe(self):\n",
    "        return f\"{self.name} is {self.colour}\"\n",
    "    \n",
    "    def exercise(self):\n",
    "        print(f\"You take {self.name} for a walk\")\n",
    "        self.energy -= 1\n",
    "            \n",
    "    def feed(self):\n",
    "        print(f\"{self.name} eats the food\")\n",
    "        self.energy += 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use this class as before by passing creating an instance of it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "our_dog = Dog(\"Spot\", \"brown\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can go ahead and interact with the dog, feeding it and exercising it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "You take Spot for a walk\n",
      "Spot eats the food\n",
      "You take Spot for a walk\n",
      "You take Spot for a walk\n"
     ]
    }
   ],
   "source": [
    "our_dog.exercise()\n",
    "our_dog.feed()\n",
    "our_dog.exercise()\n",
    "our_dog.exercise()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This all looks like it is working. But if we take a look at the dog's current energy levels:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-1\n"
     ]
    }
   ],
   "source": [
    "print(our_dog.energy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "we can see that it now has negative energy! This is not a state that makes sense and so we want to catch it so that we can avoid it happening."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Runtime checks\n",
    "\n",
    "The first thing to do is to use the tools already at our disposal to see what we can do. We know how to write conditionals to check for state so let's use those:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Dog:\n",
    "    def __init__(self, name, colour):\n",
    "        self.name = name\n",
    "        self.colour = colour\n",
    "        self.energy = 1\n",
    "    \n",
    "    def describe(self):\n",
    "        return f\"{self.name} is {self.colour}\"\n",
    "    \n",
    "    def exercise(self):\n",
    "        if self.energy >= 1:  # Added this if statement\n",
    "            print(f\"You take {self.name} for a walk\")\n",
    "            self.energy -= 1\n",
    "            \n",
    "    def feed(self):\n",
    "        print(f\"{self.name} eats the food\")\n",
    "        self.energy += 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, if we make a `Dog` and try to over-exercise it, it will only walk while it has energy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "You take Spot for a walk\n"
     ]
    }
   ],
   "source": [
    "our_dog = Dog(\"Spot\", \"brown\")\n",
    "our_dog.exercise()\n",
    "our_dog.exercise()\n",
    "our_dog.exercise()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We only get the message printed once and if we look at energy, we see it's stopped at `0`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n"
     ]
    }
   ],
   "source": [
    "print(our_dog.energy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is now preventing the bad state from being reached but unless we're really paying attention, it's easy to call the `exercise` function and think that everything is fine, even though our dog is tired.\n",
    "\n",
    "We could improve this a bit by adding an `else` clause to print a message in that situation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Dog:\n",
    "    def __init__(self, name, colour):\n",
    "        self.name = name\n",
    "        self.colour = colour\n",
    "        self.energy = 1\n",
    "    \n",
    "    def describe(self):\n",
    "        return f\"{self.name} is {self.colour}\"\n",
    "    \n",
    "    def exercise(self):\n",
    "        if self.energy >= 1:\n",
    "            print(f\"You take {self.name} for a walk\")\n",
    "            self.energy -= 1\n",
    "        else:  # Added this else statement\n",
    "            print(f\"{self.name} is tired. It doesn't want a walk.\")\n",
    "            \n",
    "    def feed(self):\n",
    "        print(f\"{self.name} eats the food\")\n",
    "        self.energy += 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "You take Spot for a walk\n",
      "Spot is tired. It doesn't want a walk.\n",
      "Spot is tired. It doesn't want a walk.\n"
     ]
    }
   ],
   "source": [
    "our_dog = Dog(\"Spot\", \"brown\")\n",
    "our_dog.exercise()\n",
    "our_dog.exercise()\n",
    "our_dog.exercise()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But even a message being printed is easy to miss, especially if it was in a larger program."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Raising exceptions\n",
    "\n",
    "In library code it's a good idea to raise an explicit error which can't be ignored by the user in this situation rather than simply print an error message to the screen and carry on. Messages like that will be missed and the error will go unnoticed, causing incorrect results.\n",
    "\n",
    "Python provides a mechanism to raise an error which cannot be ignored. These are called *exceptions*. They are useful as it allows us as programmer writing classes and functions to require the user of our code to explicitly deal with any errors and force a consistent state which we can rely on.\n",
    "\n",
    "We can make a change to our `exercise` function to, instead of printing an error message, raise an exception:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "tags": [
     "nbval-ignore-output"
    ]
   },
   "outputs": [],
   "source": [
    "class Dog:\n",
    "    def __init__(self, name, colour):\n",
    "        self.name = name\n",
    "        self.colour = colour\n",
    "        self.energy = 1\n",
    "    \n",
    "    def describe(self):\n",
    "        return f\"{self.name} is {self.colour}\"\n",
    "    \n",
    "    def exercise(self):\n",
    "        if self.energy >= 1:\n",
    "            print(f\"You take {self.name} for a walk\")\n",
    "            self.energy -= 1\n",
    "        else:\n",
    "            raise RuntimeError(f\"{self.name} is tired. It doesn't want a walk.\")  # Now we raise\n",
    "            \n",
    "    def feed(self):\n",
    "        print(f\"{self.name} eats the food\")\n",
    "        self.energy += 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, we manually *raise* a `RuntimeError`, which indicates that something is wrong with the value of one of the arguments. A list of all Python exceptions is [here](http://docs.python.org/library/exceptions.html). It is important to choose the correct exception type for the error you're reporting."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now when we call the function repeatedly, we get a sensible error message printed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "tags": [
     "raises-exception"
    ]
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "You take Spot for a walk\n"
     ]
    },
    {
     "ename": "RuntimeError",
     "evalue": "Spot is tired. It doesn't want a walk.",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mRuntimeError\u001b[0m                              Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-11-dede21eb5090>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[1;32m      1\u001b[0m \u001b[0mour_dog\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mDog\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m\"Spot\"\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m\"brown\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m      2\u001b[0m \u001b[0mour_dog\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mexercise\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m----> 3\u001b[0;31m \u001b[0mour_dog\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mexercise\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m      4\u001b[0m \u001b[0mour_dog\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mexercise\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;32m<ipython-input-10-9f3ea81cdfc7>\u001b[0m in \u001b[0;36mexercise\u001b[0;34m(self)\u001b[0m\n\u001b[1;32m     13\u001b[0m             \u001b[0mself\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0menergy\u001b[0m \u001b[0;34m-=\u001b[0m \u001b[0;36m1\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     14\u001b[0m         \u001b[0;32melse\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0;32m---> 15\u001b[0;31m             \u001b[0;32mraise\u001b[0m \u001b[0mRuntimeError\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34mf\"{self.name} is tired. It doesn't want a walk.\"\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     16\u001b[0m \u001b[0;34m\u001b[0m\u001b[0m\n\u001b[1;32m     17\u001b[0m     \u001b[0;32mdef\u001b[0m \u001b[0mfeed\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mself\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m:\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mRuntimeError\u001b[0m: Spot is tired. It doesn't want a walk."
     ]
    }
   ],
   "source": [
    "our_dog = Dog(\"Spot\", \"brown\")\n",
    "our_dog.exercise()\n",
    "our_dog.exercise()\n",
    "our_dog.exercise()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that it is telling us that the exception was raised when calling `exercise` the second time. When an exception is raised, the program will not execute any more lines of code and so it never gets to even *try* calling `exercise` a third time."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since an exception being raised will cause the function to stop running at that point, you can use `raise`s as a pre-check in your functions to enforce that the state is consistent and then just carry on ahead, assuming that everything is fine. This reduces having to nest `if` statements to check multiple things:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Dog:\n",
    "    def __init__(self, name, colour):\n",
    "        self.name = name\n",
    "        self.colour = colour\n",
    "        self.energy = 1\n",
    "    \n",
    "    def describe(self):\n",
    "        return f\"{self.name} is {self.colour}\"\n",
    "    \n",
    "    def exercise(self):\n",
    "        if self.energy <= 0:\n",
    "            raise RuntimeError(f\"{self.name} is tired. It doesn't want a walk.\")\n",
    "            \n",
    "        # If the code gets to here, we know that eveything is ok\n",
    "        print(f\"You take {self.name} for a walk\")\n",
    "        self.energy -= 1   \n",
    "            \n",
    "    def feed(self):\n",
    "        print(f\"{self.name} eats the food\")\n",
    "        self.energy += 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This conforms better to the [*Zen of Python*](https://www.python.org/dev/peps/pep-0020/)'s fifth koan:\n",
    "\n",
    "> Flat is better than nested."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Catching exceptions\n",
    "\n",
    "Being able to raise exceptions is one half of the story but it becomes really useful when we can *handle* incoming errors. Sometimes we've called a function and while *it* doesn't know how to deal with what you've given it, *you* might. So what you can do is *catch* the raised exception and handle it.\n",
    "\n",
    "The simplest version of this is a situation where the function failing isn't a problem for your code, you just want to be able to log a message that it happened and carrry on:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Something went wrong while trying to exercise the dog\n"
     ]
    }
   ],
   "source": [
    "try:\n",
    "    our_dog.exercise()\n",
    "except RuntimeError:\n",
    "    print(\"Something went wrong while trying to exercise the dog\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we used a `try`/`except` block. You put the code that you want to run in the `try` block and any exceptions that are raised can be caught in the `except` block. Here we know that our function may raise a `RuntimeError` so we catch it and print a relevant message.\n",
    "\n",
    "We've converted a noisy error on the screen to a custom message.\n",
    "\n",
    "However, we've lost here the message that was raised in the first place (`\"Spot is tired. It doesn't want a walk.\"`) and so perhaps our new message is less useful that the messy error. We can get the power of both by including the the caught exception into our log message by catching it with `except RuntimeError as e` and including `e` in our printed message:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Something went wrong while trying to exercise the dog: Spot is tired. It doesn't want a walk.\n"
     ]
    }
   ],
   "source": [
    "try:\n",
    "    our_dog.exercise()\n",
    "except RuntimeError as e:\n",
    "    print(f\"Something went wrong while trying to exercise the dog: {e}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes we're able to do something about an error. In the case that the dog is tired, we can feed it and it will gain energy. You can run any code you like in an `except` block so we can do the recovery and retry there:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Spot eats the food\n",
      "You take Spot for a walk\n"
     ]
    }
   ],
   "source": [
    "try:\n",
    "    our_dog.exercise()\n",
    "except RuntimeError as e:\n",
    "    our_dog.feed()\n",
    "    our_dog.exercise()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should try to mimimise the amount of code you place in an `except` block but it's a very useful way to recover from errors."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "- Edit your code in `encode.py` to read:\n",
    "  ```python\n",
    "  from morse import EnglishMessage\n",
    "\n",
    "  message = \"SOS We have hit an iceberg and need help quickly\"\n",
    "  english_message = EnglishMessage(message)\n",
    "\n",
    "  encoded_message = english_message.encode()\n",
    "\n",
    "  print(f\"Incoming message: {message}\")\n",
    "  print(f\"   Morse encoded: {encoded_message}\")\n",
    "  ```\n",
    "- Change the message to include letters that cannot be encoded such as \"&\", \"£\" and \"!\".\n",
    "- Put the `english_message.encode()` call in a `try` block and print a useful error message. (Hint: there are exception types other than `RuntimeError`, see the [documentation](https://docs.python.org/3/library/exceptions.html).\n",
    "- Check that if the message string is normal, the error message is *not* printed.\n",
    "- [<small>answer</small>](answer_morse_exceptions.ipynb)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
