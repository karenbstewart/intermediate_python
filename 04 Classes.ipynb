{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Classes and objects\n",
    "\n",
    "You're perhaps starting to see a theme in this workshop so far. By introducing functions and then modules we're creating reusable units which have defined interfaces (import *this* module, call *this* function with *these* arguments) which make it easier for people to use our code.\n",
    "\n",
    "We've used functions to package up code which *does* things but as you saw when moving the code into the `morse` module, there's also code in there which doesn't exist to be called by a user but is rather just creating data which is used by the functions. The two are logically linked (the function won't work without the data and the data isn't useful without the functions).\n",
    "\n",
    "Python has a feature which allows us to combine code and data together into a single object which contains everything it needs to do anything we ask of it, these are *classes*. Classes are a way of creating a \"template\" which is then used to create *objects* which you can interact with.\n",
    "\n",
    "With functions we were giving names to actions (or verbs) but with classes we can give names to *concepts*.\n",
    "\n",
    "Image that we want to write some code to represent our pet dog, Spot. The dog has a name and a colour. Based on the tools we know so far, we might represent this as a dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "our_dog = {\"name\": \"Spot\", \"colour\": \"brown\"}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The problem with a dictionary, however, is that if you're passing it to a function you have to be very careful to ensure that it has all the keys correctly set that the function expects. For example, if there were a function `describe` which looks like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Spot is brown\n"
     ]
    }
   ],
   "source": [
    "def describe(dog):\n",
    "    return f\"{dog['name']} is {dog['colour']}\"\n",
    "\n",
    "print(describe(our_dog))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "then you would need to ensure that any data that was passed to it had the keys `\"name\"` and `\"colour\"`.\n",
    "\n",
    "We can use classes to create a new type of data which represent all dogs and we can ensure that all data of this type always have the `name` and `colour` attributes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Starting our class\n",
    "\n",
    "We would represent this as a class with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Dog:\n",
    "    def __init__(self, name, colour):\n",
    "        self.name = name\n",
    "        self.colour = colour"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This has created a new data type called `Dog` which we can create instances of with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "our_dog = Dog(\"Spot\", \"brown\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can ask for information from that instance using the dot syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Spot\n"
     ]
    }
   ],
   "source": [
    "print(our_dog.name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`Dog` and `our_dog` are different kind of thing. `Dog` is called a *class* and can be seen as a template for creating dog *objects*. `our_dog` is one such object.\n",
    "\n",
    "You can make multiple objects from a class, for example we can make a second dog, Fido with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "other_dog = Dog(\"Fido\", \"grey\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and get information about *that* dog with the same attribute names on the new object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "grey\n"
     ]
    }
   ],
   "source": [
    "print(other_dog.colour)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This means that we can tweak our `describe` function to accept a `Dog` object and we can trust that it will always work. no matter which `Dog` we pass to it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Spot is brown\n"
     ]
    }
   ],
   "source": [
    "def describe(dog):\n",
    "    return f\"{dog.name} is {dog.colour}\"\n",
    "\n",
    "print(describe(our_dog))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## What is `__init__`?\n",
    "\n",
    "`__init__` is a function. It is a special function and is sometimes called the *constructor* or *initialiser*. It must be present in all classes, and constructors are used in all object orientated programming languages.\n",
    "\n",
    "The job of the constructor is to set up the initial state of an object. In this case, you can see that the constructor creates two variables:\n",
    "\n",
    "- `name` which will hold the name of the dog\n",
    "- and `colour` which will hold a string describing the colour of the dog.\n",
    "\n",
    "Note that the variables are defined as attached to `self`, via the full stop, e.g. `self.name`. `self` is a special variable that is only available within the functions of the class and provides access to the current *object* that we are talking about. There is a full explanation of how `self` works below.\n",
    "\n",
    "`__init__` is called automatically when an object of that class is created. In our case, when we call `Dog(...)` it will call `__init__` for us.\n",
    "\n",
    "The first time we called `Dog(\"Spot\", \"brown\")`, `self` was referring at the object we were putting at `our_dog` so `self.name` is referring to the same thing as `our_dog.name`. The second time we called it it was referring to the object at `other_dog`.\n",
    "\n",
    "Note that while `self` is written in function definition as if it were a parameter of `__init__`, we don’t need to pass it ourselves. `self` is passed implicitly by Python when we construct an object of the class."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Class functions\n",
    "\n",
    "At the moment, `Dog` isn't giving us any real benefit above using a dictionary. It's still just a container for data. One of the benefits of classes is being able to combine data and functionality in one place."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Making `Dog` as a class means that we can trust that if we pass a `Dog` to `describe` that it will definitely work but this has left us in a position where our data and functions are separate and we have to remember that `describe` takes a `Dog` object as its argument.\n",
    "\n",
    "We can solve this by enforcing that the `describe` function can only ever be called on a `Dog` object by moving the function inside the class.\n",
    "\n",
    "To move this function so that it is a part of the class we do two things:\n",
    "\n",
    "1. move the lines of code into the class, indenting it appropriately\n",
    "2. replace `dog` with `self`\n",
    "\n",
    "this give us:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Dog:\n",
    "    def __init__(self, name, colour):\n",
    "        self.name = name\n",
    "        self.colour = colour\n",
    "    \n",
    "    def describe(self):\n",
    "        return f\"{self.name} is {self.colour}\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since we have changed the code defining what a `Dog` is, we need to recreate our objects so that they know about the changes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "our_dog = Dog(\"Spot\", \"brown\")\n",
    "other_dog = Dog(\"Fido\", \"grey\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now call the describe function on each `Dog` object using the dot syntax:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Spot is brown\n"
     ]
    }
   ],
   "source": [
    "print(our_dog.describe())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Fido is grey\n"
     ]
    }
   ],
   "source": [
    "print(other_dog.describe())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When a function has been moved inside a class like this, it is sometimes referred to as a *method* but I use both terms.\n",
    "\n",
    "There is now no way to call this function (or method) on any object which is not a `Dog`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Adding variable state"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Having the bare facts about the dog is useful but we want to be able to make it a living, breathing thing. Let's introduce the concept of \"energy\" for the dog. It will be a number which increases when we feed it and decreases when we exercise it.\n",
    "\n",
    "Previously we added all object attributes as arguments to `__init__` and then assigned them to `self` with `self.name = `. It is perfectly possible to set object attributes statically, without having them depend on the arguments that were passed in.\n",
    "\n",
    "For example, we want our dog to have `energy` as an attribute. Let's decide that by default, all `Dog`s have an energy of `1` when they are first created. We can assign the variable `self.energy` in `__init__`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Dog:\n",
    "    def __init__(self, name, colour):\n",
    "        self.name = name\n",
    "        self.colour = colour\n",
    "        self.energy = 1  # This is the only new line\n",
    "    \n",
    "    def describe(self):\n",
    "        return f\"{self.name} is {self.colour}\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have `energy` as an attribute we can go ahead and write a function which uses it. We want our dog to be able to take our dog for a walk which will use up energy. We add another method to the class called `exercise`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Dog:\n",
    "    def __init__(self, name, colour):\n",
    "        self.name = name\n",
    "        self.colour = colour\n",
    "        self.energy = 1\n",
    "\n",
    "    def describe(self):\n",
    "        return f\"{self.name} is {self.colour}\"\n",
    "    \n",
    "    def exercise(self):\n",
    "        print(f\"You take {self.name} for a walk\")\n",
    "        self.energy -= 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See that in the `exercise` function we are accessing the energy with `self.energy`.\n",
    "\n",
    "We can test that this is working by recreating our dog instance and seeing how calling `exercise` affects the dog's energy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [],
   "source": [
    "our_dog = Dog(\"Spot\", \"brown\")\n",
    "other_dog = Dog(\"Fido\", \"grey\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\n"
     ]
    }
   ],
   "source": [
    "print(our_dog.energy)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "You take Spot for a walk\n"
     ]
    }
   ],
   "source": [
    "our_dog.exercise()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0\n"
     ]
    }
   ],
   "source": [
    "print(our_dog.energy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After calling the `exercise` function, we can see that Spot's energy has reduced by one.\n",
    "\n",
    "But note that Fido's energy has not been affected:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\n"
     ]
    }
   ],
   "source": [
    "print(other_dog.energy)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's complete the story by also implementing a function which we can use to feed our dog to give it energy:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Dog:\n",
    "    def __init__(self, name, colour):\n",
    "        self.name = name\n",
    "        self.colour = colour\n",
    "        self.energy = 1\n",
    "    \n",
    "    def describe(self):\n",
    "        return f\"{self.name} is {self.colour}\"\n",
    "    \n",
    "    def exercise(self):\n",
    "        print(f\"You take {self.name} for a walk\")\n",
    "        self.energy -= 1\n",
    "            \n",
    "    def feed(self):\n",
    "        print(f\"{self.name} eats the food\")\n",
    "        self.energy += 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## About `self`\n",
    "\n",
    "The existence and purpose of the `self` parameter in Python classes is often the most confusing thing when learning about them. To get to the point of seeing why they work how they do, there's a few things to clarify.\n",
    "\n",
    "The code that we write that defines our class can be thought of as a template which will be used to create by every instance (object) of that class. This means that any code we write in there has to work and make sense for *all* objects that were made from the template. The functions we define in there therefore also need to be generic and work on all objects made from the class.\n",
    "\n",
    "If a function is generic, how can it know that at one point in your program it's being called on object A and and some later time being called on object B? We do this by accepting to the function, as an parameter, the object that we're calling it on. It is *this* that `self` is referring to.\n",
    "\n",
    "To show this in action, let's walk through an example:\n",
    "\n",
    "We can construct as many instances (objects) of a class as we want, and each will have its own `self` and its own set of attributes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [],
   "source": [
    "our_dog = Dog(\"Spot\", \"brown\")\n",
    "other_dog = Dog(\"Fido\", \"grey\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we describe `our_dog` we get:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'Spot is brown'"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "our_dog.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "but calling the same function on the other dog gives us a different result:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'Fido is grey'"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "other_dog.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's go thorough that more slowly and see what `self` is doing along the way.\n",
    "\n",
    "First we called the `Dog` class like a function, passing it two arguments and assigned it to the variable `our_dog`:\n",
    "\n",
    "```python\n",
    "our_dog = Dog(\"Spot\", \"brown\")\n",
    "```\n",
    "\n",
    "When you call a class like this, it makes a new object from the template and automtically calls the `__init__` function behind the scenes. As we saw before, our `__init__` has three parameters, `self`, `name` and `colour`. Python, when calling `__init__` will automatically pass in our object (`our_dog` in this case) as the first argument and so inside the function, `self` is referring to `our_dog`, our newly created object.\n",
    "\n",
    "Therefore, we can imagine that our function which looks like:\n",
    "\n",
    "```python\n",
    "def __init__(self, name, colour):\n",
    "    self.name = name\n",
    "    self.colour = colour\n",
    "    self.energy = 1\n",
    "```\n",
    "\n",
    "is being treated like:\n",
    "\n",
    "```python\n",
    "self = our_dog\n",
    "name = \"Spot\"\n",
    "colour = \"brown\"\n",
    "\n",
    "self.name = name\n",
    "self.colour = colour\n",
    "self.energy = 1\n",
    "```\n",
    "\n",
    "and so here, `self.name = name` is effectively doing `our_dog.name = \"Spot\"`.\n",
    "\n",
    "The same thing happens when we create `other_dog` with:\n",
    "\n",
    "```python\n",
    "other_dog = Dog(\"Fido\", \"grey\")\n",
    "```\n",
    "\n",
    "We now know that due to the `__init__` function operating on `self`, we now have two dogs where `our_dog.name` is `\"Spot\"` and `other_dog.name` is `\"Fido\"`.\n",
    "\n",
    "Now that both of our objects have been fully created, we're ready to start interacting with them by calling some functions.\n",
    "\n",
    "When we call the `describe` function,  a similar process occurs:\n",
    "\n",
    "```python\n",
    "our_dog.describe()\n",
    "```\n",
    "\n",
    "Since the `our_dog` object was made from the `Dog` class, `our_dog.describe` is referring to the `describe` function inside that class:\n",
    "\n",
    "```python\n",
    "class Dog:\n",
    "    ...\n",
    "    def describe(self):\n",
    "        return f\"{self.name} is {self.colour}\"\n",
    "```\n",
    "\n",
    "and so\n",
    "\n",
    "```python\n",
    "our_dog.describe()\n",
    "```\n",
    "\n",
    "it is effectively doing\n",
    "\n",
    "```python\n",
    "Dog.describe(our_dog)\n",
    "```\n",
    "\n",
    "and passing whatever object the `describe` function was called on as the first argument. Again, inside the function this is called `self` so when we do\n",
    "\n",
    "```python\n",
    "f\"{self.name} is {self.colour}\"\n",
    "```\n",
    "\n",
    "it is essentially doing\n",
    "\n",
    "```python\n",
    "f\"{our_dog.name} is {our_dog.colour}\"\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To summarise, the `self` parameter in class functions points at the object that the function was called on. The programmer calling the function does not pass the argument explicitly, it is done automatically by Python. This allows you to store data in one function (e.g. in `__init__` doing `self.name = name`) and use it in another (e.g. in `describe` doing `f\"{self.name} is {self.colour}\"`)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": [
     "exercise"
    ]
   },
   "source": [
    "### Exercise\n",
    "\n",
    "- Edit your `morse.py` script and change it so that instead of containing bare functions, you create two classes.\n",
    "    1. Create a class which will represent the concept of a message written in English. The class should be called `EnglishMessage` and it should have an `encode` function. Its constructor should take the english message string as an agument and save it into `self` as `self.message`. `letter_to_morse` should be moved into the constructor and saved as `self.letter_to_morse`. `encode` should then read `self.message` and `self.letter_to_morse` and perform the same processing as we have already done.\n",
    "    2. The other class should be called `MorseMessage` and it should have a `decode` function. It will represent a message written in Morse Code. Its constructor should accept a string argument of the encoded morse message and also create a variable called `self.morse_to_letter`.\n",
    "- When you have finished, test that the Morse code produced by your class is correctly translated back to English. Edit `test_morse.py` to read:\n",
    "  ```python\n",
    "  from morse import EnglishMessage, MorseMessage\n",
    "\n",
    "  message_string = \"hello world\"\n",
    "  message = EnglishMessage(message_string)\n",
    "\n",
    "  code_string = message.encode()\n",
    "  code = MorseMessage(code_string)\n",
    "  decoded = code.decode()\n",
    "\n",
    "  print(decoded == message_string)\n",
    "  ```\n",
    "  When run, it should print `True`, showing that your `MorseTranslator` class can encode and decode the message.\n",
    "- Feel free to look at [the answer](answer_morse_class.ipynb) to guide you."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Naming\n",
    "\n",
    "You may have noticed a few different styles of naming used for things so far. We've been calling our data variables things like `letter_to_morse` while our class was called `MorseTranslator`. The first style with all lower case letter and underscores to separate words is known as snake case and the style with upper case letters at the start of words but no spcaes between them is known as camel case.\n",
    "\n",
    "Python has a document called [PEP-8](https://www.python.org/dev/peps/pep-0008/) which contains suggestions on how to format and name your code and it suggests:\n",
    "- variables: snake case like `letter_to_morse` or `message`\n",
    "- functions: snake case like `encode` or `add_arrays`\n",
    "- modules: snake case like `morse` or `arrays`\n",
    "- classes: camel case like `MorseTranslator` or `Dog`\n",
    "\n",
    "These are just suggestions and while they are followed by the majority of Python projects, if you are contributing to an existing Python project then you should follow their internal code style."
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
